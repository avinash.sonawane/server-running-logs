Assignment No.1 
Server Running logs Display Realtime.

Made By - Avinash Sonawane.

Steps : -
1.Unzip the file.
2.Make sure you have node js is installed or not.
3.Go to directory path from your terminal.
4.Type npm install and wait for dependencies to install.
5.Type npm start and hit it.
6.After Running your Server Open browser and enter URL http://localhost:3000
7.For see running logs click on tab Assignment.

For Test- (After Running your Server)
we have four data interfaces.
base URL - "http://localhost:3000/product"

1. "/" - to get all products. (GET)
2. "/getProduct" - to get specific product by its id. (GET)
    ex. /product/getProduct?prodId=1
3. "/createProduct" - to create a new product. (POST)
    ex. /product/createProduct
    Post Data - {
                    "id" : 5,
                    "name" : "test5",
                    "prize" : 500,
                    "code" : "t5",
                    "city" : "Mumbai"
                }
4. "/delProduct" - to delete product from list. (DELETE)
    ex. /product/delProduct?prodId=1

To see the data please refer productData.json and to see the logs please refer apiLog.txt.

Hit the API's and Test it in Assignment Tab.

Thanks. have a good day.