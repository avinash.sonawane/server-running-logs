const fs = require('fs');
module.exports = {
    genlog : function(req, res, next){
        var apiDate = new Date();
        var ip;
        var msg = "API Called";
        console.log(req.originalUrl);
        if(req.connection.remoteAddress == '::1'){
            ip = '127.0.0.1';
        }else{
            ip = req.connection.remoteAddress;
        }
        var data = req.originalUrl+","+ip+","+msg+","+apiDate+"\n";
        fs.appendFile("./apiLog.txt", data, function(err) {
            if(err) {
                return console.log(err);
            }
            next();
        });         
    }
};