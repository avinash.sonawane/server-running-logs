var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});

router.get('/assignment', function(req, res, next) {
  res.render('logs');
});

module.exports = router;
