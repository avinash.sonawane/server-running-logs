var express = require('express');
const fs = require('fs');
var async = require('async');
var router = express.Router();
var loggen = require('../middleware/logger');
const stats = fs.statSync("./productData.json");

/* GET Product listing. */
router.get('/', loggen.genlog, function (req, res) {
  if (stats.size && stats.size > 0) {
    fs.readFile('./productData.json', function (err, data) {
      if (err) {
        res.json(err);
      } else {
        try {
          const items = JSON.parse(data);
          res.json(items);
        } catch (e) {
          res.json("JSON Format Error");
        }
      }
    });
  } else {
    res.json("File Empty");
  }
});

router.get('/getProduct', loggen.genlog, function (req, res) {
  var prodId = req.query.prodId;
  if (stats.size && stats.size > 0) {
    fs.readFile('./productData.json', function (err, data) {
      const item = JSON.parse(data);
      var result = {};
      async.waterfall([
        function (callback) {
          async.forEach(item, function (prod, callback) {
            if (prod.id == prodId) {
              result = prod;
            }
            callback();
          })
          callback();
        }
      ], function () {
        res.json(result);
      })
    });
  } else {
    res.json("File Empty");
  }
});

router.post('/createProduct', loggen.genlog, function (req, res) {
  var json = req.body;
  if (stats.size && stats.size > 0) {
    fs.readFile('./productData.json', function (err, data) {
      if (err) {
        res.json(err);
      } else {
        try {
          const items = JSON.parse(data);
          items.push(json);
          const dataRes = JSON.stringify(items);
          fs.writeFile('./productData.json', dataRes, 'utf8', function (err, result) {
            if (err) {
              res.json(err);
            } else {
              res.json("Record Inserted");
            }
          });
        } catch (e) {
          res.json("JSON Format Error");
        }
      }
    });
  } else {
    res.json("File Empty");
  }
});

router.delete('/delProduct', loggen.genlog, function (req, res) {
  var prodId = req.query.prodId;
  var index;
  if (stats.size && stats.size > 0) {
    fs.readFile('./productData.json', function (err, data) {
      const item = JSON.parse(data);
      async.waterfall([
        function (callback) {
          async.forEach(item, function (prod, callback) {
            //console.log(prod);
            if (prod.id == prodId) {
              index = item.indexOf(prod);
            }
            callback();
          })
          callback();
        }
      ], function () {
        item.splice(index, 1);
        const dataRes = JSON.stringify(item);
        fs.writeFile('./productData.json', dataRes, 'utf8', function (err, result) {
          if (err) {
            res.json(err);
          } else {
            res.json("Item Deleted");
          }
        });
      })
    });
  } else {
    res.json("File Empty");
  }
});

router.get('/getLogs', function (req, res) {
  var attr = req.query.attr;
  const stats = fs.statSync("./apiLog.txt");
  var result = [];
  var i = 1;
  if (stats.size && stats.size > 0) {
    fs.readFile('./apiLog.txt', "utf8", function (err, data) {
      if (err) {
        res.json(err);
      } else {
        data = data.trim();
        var rows = data.split("\n");
        // if(rows.length > attr){
        //   rows = rows.slice(attr);
        // }else{
        //   rows = rows;
        // }
        for (var j = rows.length - 1; j > 0; j--) {
          var splitString = rows[j].split(",");

          result.push({
            "url": splitString[0],
            "ip": splitString[1],
            "msg": splitString[2],
            "apiDate": splitString[3],
          });
        }
        res.json(result);
      }
    });
  } else {
    res.json("File Empty");
  }
});

module.exports = router;
